﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class Movement : MonoBehaviour
{

	public class spawnStruct
	{
		public int x {get; set;}
		public int y {get; set;}
		public float angX {get; set;}
		public float angZ {get; set;}
		public Transform model {get; set;}
		public float time {get; set;}

		public spawnStruct(int x, int y, float angX, float angZ, Transform model, float time){
			this.x = x;
			this.y = y;
			this.angX = angX;
			this.angZ = angZ;
			this.model = model;
			this.time = time;
		}

	}

	//public StraightPath straightPath;

	public float speed;
	public float oncomingAircraftSpeed;
	public int spawnDist;
	public Transform prefab;
	
	public Rigidbody plane1;
	public Transform spawn1;
	//public GameObject spawn;
	private StraightPath path;
	public AircraftData oncomingAircraftData;

	public float startTime;

	public Queue<spawnStruct> spawnQueue = new Queue<spawnStruct>();

	//private int spawnIndex = 0;

	// Use this for initialization
	void Start()
	{ 
	}

	public void init(){
		//spawnStruct spawnStruct1 = new spawnStruct ();
		//spawnStruct.x = 30;
		spawnQueue.Enqueue (new spawnStruct  (0, 0, 50f, 10f, prefab, 0));
		spawnQueue.Enqueue (new spawnStruct  (0, 0, 50f, 10f, prefab, 50));
		spawnQueue.Enqueue (new spawnStruct  (30, 30, 50f, 10f, prefab, 50*2));
		spawnQueue.Enqueue (new spawnStruct  (30, 30, 50f, 10f, prefab, 50*3));
		spawnQueue.Enqueue (new spawnStruct  (30, 30, 50f, 10f, prefab, 50*4));
		spawnQueue.Enqueue (new spawnStruct  (30, 30, 50f, 10f, prefab, 50*5));

		startTime = Time.realtimeSinceStartup;

		plane1 = GetComponent<Rigidbody>();
		Vector3 vel = new Vector3(speed / transform.localScale.x, 0, 0);
		plane1.velocity = transform.TransformVector(vel);
		
		//spawn (30, 30, 50f, 10f, prefab, 0);
		//spawn (spawnList [0]);
		spawn (spawnQueue.Dequeue ());
	}

	//public void spawn(int x, int y, float angX, float angZ, Transform model, float t){
	public void spawn(spawnStruct ss){
		Vector3 spawnLoc = generateLoc(spawnDist, ss.angX, ss.angZ);
		Vector3 offset = new Vector3(ss.x, ss.y, 0);
		path = (StraightPath)prefab.GetComponent("StraightPath");
		path.speed = oncomingAircraftSpeed;
		spawn1 = GameObject.Instantiate(ss.model, spawnLoc, getHeading(spawnLoc, offset)) as Transform;
	}
	
	// Update is called once per frame

	void FixedUpdate()
	{
		if (spawn1 != null && Vector3.Distance (spawn1.position, plane1.position) <= 300) {
			//print("Destroy");
			Destroy(spawn1.gameObject);
		}



	}

	void Update()
	{
		float currentTime = Time.realtimeSinceStartup;

		if(spawnQueue.Peek() != null){
			//print(spawnQueue.Peek().time);
			//print(currentTime);
			if(spawnQueue.Peek().time <= currentTime){
				spawn(spawnQueue.Dequeue());
			}
		}

	}
	
	public Vector3 getPos(float time)
	{
		Vector3 currentV = plane1.velocity;
		currentV *= time;
		return (currentV + plane1.position);
	}
	
	public Quaternion getHeading(Vector3 loc, Vector3 offset)
	{
		Vector3 tarPos = plane1.position;
		for (int i = 0; i < 100; i++)
		{
			float dist = Vector3.Distance(tarPos, loc);
			float time = dist / path.speed; // This 200 is dependant on the spawn's speed
			tarPos = getPos(time); 
		}
		
		Quaternion ret = new Quaternion(1, 1, 1, 1);
		ret.SetFromToRotation(new Vector3(1,0,0), (tarPos+offset)-loc); // this should shoot for where the plane spawns!
		return ret;
	}
	
	public Vector3 generateRandLoc(float dist, float y, float z)
	{
		Vector3 dest = new Vector3(dist, 0f, 0f);
		Vector3 rand = new Vector3(0f, ((UnityEngine.Random.value - 0.5f) * 2 * Mathf.PI * y/180), (((UnityEngine.Random.value) - 0.5f) * 2 * Mathf.PI * z / 180));
		
		dest = RotateVector(dest, rand);
		
		return dest + plane1.position;
	}


	public Vector3 generateLoc(float dist, float y, float z){
		Vector3 dest = new Vector3(dist, 0f, 0f);
		Vector3 ang = new Vector3(0f, ((y) * Mathf.PI /180), (Mathf.PI * z / 180));
		
		dest = RotateVector(dest, ang);
		
		return dest + plane1.position;
	}

	
	public Vector3 RotateX(Vector3 v, float angle)
	{
		float sin = Mathf.Sin(angle);
		float cos = Mathf.Cos(angle);
		
		float ty = v.y;
		float tz = v.z;
		v.y = (cos * ty) - (sin * tz);
		v.z = (cos * tz) + (sin * ty);
		
		return v;
	}
	
	public Vector3 RotateY(Vector3 v, float angle)
	{
		float sin = Mathf.Sin(angle);
		float cos = Mathf.Cos(angle);
		
		float tx = v.x;
		float tz = v.z;
		v.x = (cos * tx) + (sin * tz);
		v.z = (cos * tz) - (sin * tx);
		
		return v;
	}
	
	public Vector3 RotateZ(Vector3 v, float angle)
	{
		float sin = Mathf.Sin(angle);
		float cos = Mathf.Cos(angle);
		
		float tx = v.x;
		float ty = v.y;
		v.x = (cos * tx) - (sin * ty);
		v.y = (cos * ty) + (sin * tx);
		
		return v;
	}
	
	public Vector3 RotateVector(Vector3 v, Vector3 angles)
	{
		v = RotateX(v, angles.x);
		v = RotateY(v, angles.y);
		v = RotateZ(v, angles.z);
		return v;
	}
}
